'''Operações matematicas na area de saude'''

def calcular_imc(peso, altura):
    '''Formula do calculo para IMC
    Chame a função calcular_imc(peso, altura) para executar
    Ex.: calcular_imc(80, 1.80)
    '''
    return peso/(altura ** 2)