'''Operação algébricas'''

def soma(valor1, valor2):
    '''
       A função soma recebe dois argumentos
       De acordo com o exemplo abaixo
       função soma(4, 5) retorna 9 
    '''
    return valor1 + valor2